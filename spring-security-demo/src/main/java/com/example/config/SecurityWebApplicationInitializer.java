package com.example.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author KhoiDV
 * @date 3/5/2019
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
