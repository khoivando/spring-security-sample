<%--
  Created by IntelliJ IDEA.
  User: KhoiDV
  Date: 3/5/2019
  Time: 3:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Access Denied</title>
</head>
<body>
    <h2>Access Denied - You are not authorized to access this resource.</h2>
    <hr>
    <a href="${pageContext.request.contextPath}/">Back to Home Page</a>
</body>
</html>
