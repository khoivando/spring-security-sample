<%--
  Created by IntelliJ IDEA.
  User: KhoiDV
  Date: 3/5/2019
  Time: 3:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
	<title>Company Home Page</title>
</head>
<body>
	<h2>Company Home Page</h2>
	<hr>
	<p>Welcome to the home page!</p>

	<p>
		User: <security:authentication property="principal.username" /><br>
		Roles: <security:authentication property="principal.authorities" />
	</p>

	<security:authorize access="hasAnyRole('MANAGER')">
	<p><a href="${pageContext.request.contextPath}/leaders">Leadership Meeting</a> (Only for MANAGER peeps)</p>
	</security:authorize>

	<security:authorize access="hasAnyRole('ADMIN')">
	<p><a href="${pageContext.request.contextPath}/systems">IT Systems Meeting</a> (Only for ADMIN peeps)</p>
	</security:authorize>

	<hr>
	<form:form action="${pageContext.request.contextPath}/logout" method="post">
		<input type="submit" value="Logout" />
	</form:form>
</body>
</html>