package com.example.bootweblogin;

import com.example.bootweblogin.model.User;
import com.example.bootweblogin.repository.RoleRepository;
import com.example.bootweblogin.repository.UserRepository;
import com.example.bootweblogin.service.UserService;
import com.example.bootweblogin.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author KhoiDV
 * @date 2/21/2019
 */
public class UserServiceTest {

    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private RoleRepository mockRepository;
    @Mock
    private BCryptPasswordEncoder mockPasswordEncoder;

    private UserService service;
    private User user;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new UserServiceImpl(mockUserRepository, mockRepository, mockPasswordEncoder);
        user = User.builder()
                .id(1)
                .name("khoidv")
                .password("password")
                .email("khoivando@gmail.com")
                .build();

        when(mockUserRepository.save(any())).thenReturn(user);
        when(mockUserRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(user));
    }

    @Test
    public void testSaveUser() {
        final String email = "khoivando@gmail.com";
        final User user = service.save(User.builder().build());
        assertEquals(email, user.getEmail());
    }

    @Test
    public void testFindUserByEmail() {
        final String email = "khoivando@gmail.com";
        final User user = service.findUserByEmail(email);
        assertEquals(email, user.getEmail());
    }

}
