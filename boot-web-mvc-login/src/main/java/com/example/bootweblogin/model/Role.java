package com.example.bootweblogin.model;

import lombok.Data;

import javax.persistence.*;

/**
 * @author KhoiDV
 * @date 2/21/2019
 */
@Data
@Entity
@Table(name = "role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private int id;

    @Column(name = "role")
    private String role;

}
