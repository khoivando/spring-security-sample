package com.example.bootweblogin.service;

import com.example.bootweblogin.model.User;

/**
 * @author KhoiDV
 * @date 2/21/2019
 */
public interface UserService {

    User findUserByEmail(String email);

    User save(User user);

}
