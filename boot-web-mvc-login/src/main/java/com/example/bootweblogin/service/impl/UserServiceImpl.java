package com.example.bootweblogin.service.impl;

import com.example.bootweblogin.model.Role;
import com.example.bootweblogin.model.User;
import com.example.bootweblogin.repository.RoleRepository;
import com.example.bootweblogin.repository.UserRepository;
import com.example.bootweblogin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

/**
 * @author KhoiDV
 * @date 2/21/2019
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User findUserByEmail(String email) {
        Optional<User> optional = userRepository.findByEmail(email);
        if(optional.isPresent()) return optional.get();
        return null;                
    }

    @Override
    public User save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        Role role = roleRepository.findByRole("ADMIN").get();
        user.setRoles(new HashSet<Role>(Arrays.asList(role)));
        userRepository.save(user);
        return user;
    }
}
