package com.example.bootweblogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootWebLoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootWebLoginApplication.class, args);
    }

}
